#!/bin/bash

systemctl stop firewalld
setenforce 0

yum install nginx -y
cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf-copia
cd /etc/nginx/ && rm -rf nginx.conf
mkdir -p /var/www/virtualhost

cat > nginx.conf << EOF
http {
	server {
		listen 8080;
		root /var/www/virtualhost/;
	}
}

events { } 
EOF

cd

mkdir -p /var/www/virtualhost/mueblesjimmy.com
mkdir -p /var/www/virtualhost/pastelesjuani.com

cat > index.html << EOF
Este es le sitio web de MueblesJimmy
EOF

mv index.html /var/www/virtualhost/mueblesjimmy.com/

cat > index.html << EOF
Este es le sitio web de PastelesJuani
EOF

mv index.html /var/www/virtualhost/pastelesjuani.com/

echo -e "Porfavor dirigirse a su navegador y poner http://ip-del-host:8080/mueblesjimmy.com/
y luego lo mismo con el otro sitio  http://ip-del-host:8080/pastelesjuani.com/ ,gracias"
